# Authentification coté Angular

## Formulaire de login en front
1. Créer un  projet angular qu'on va appeler angular-auth, avec routing
2. Dans le AppModule rajouter le HttpClientModule et le FormsModule
3. Créer un fichier entities.ts et dedans, mettre une interface User dedans qui reprend les champs de l'entité côté back
4. Créer un component LoginPage et dedans faire un formulaire avec un champ email et un champ password et le lier avec une propriété avec un ngModel

## Exercice :
1. Rajouter une route en GET qui permet de récupérer tous les utilisateurs sur le projet BACK, sur /api/user/all
2. Dans le projet Angular, ajouter un component AllUsers qui va afficher cette liste dans un tableau HTML classique; dans le tableau, bien utiliser les balises thead, th, tbody, etc
3. Dans le template Angular du composant AllUsers, afficher le tableau ssi il y a des users dans la liste
4. Dans le projet Java, modifier la class auth.SecurityConfig pour n'autoriser que les utilisateurs ayant le rôle "ROLE_ADMIN" à accéder au endpoint /api/user/all 

## Exercice 2 :
1. Dans le projet Back, créer une branche dog, se basculer dessus, et la pusher vers le serveur
2. Dans le projet Front, créer une branche dog, se basculer dessus, et la pusher vers le serveur
3. Dans le projet Back, créer une entité Dog {id (auto-généré), name (string), breed (string) }
4. Modifiez l'entité User pour que chaque User puisse posséder plusieurs chiens (relation OneToMany)
5. Dans le projet Front, créer l'entité Dog
6. Dans le projet Front angular, modifiez le home.component pour afficher la liste des chiens de l'utilisateur connecté, en plus de son email
7. Dans le projet Back, créer le repo et le controller associés à l'entité Dog; le controller doit avoir 1 route : DELETE sur /api/dog/{id} qui supprime un chien
8. Dans le projet Front, rajouter à côté de chaque chien un bouton permettant de le supprimer (il faudra aussi créer le service qui permet de faire la requête vers le back)
9. Dans le projet Back, faites en sorte que l'utilisateur puisse seulement supprimer des chiens qui lui appartiennent 

## Promouvoir un user en admin
1. Créer un contrôleur AdminController dont l'url de base sera /api/admin et dedans, mettre le /user/all que vous aviez mis dans le UserController (et on va juste l'appeler /api/admin/user)
2. Modifier le SecurityConfig pour faire que toutes les routes qui commencent par /api/admin ne soient accessibles qu'aux personnes ROLE_ADMIN
3. Créer une méthode PATCH dans le AdminController, sur la route /api/admin/user/{id}/role/{role}, cette méthode va récupérer le user par son id en utilisant le repository, puis va lui assigner le deuxième argument role en string, comme nouveau role
4. Côté angular, modifier le UserService pour mettre à jour la route pour afficher tous les users
5. Rajouter une méthode changeRole dans ce UserService qui attendra en argument l'id d'un user et le rôle à lui assigner et qui enverra ça sur l'url définie au dessus
6. Dans le composant avec votre list de user, modifier la case rôle pour faire que ça soit un select permettant de choisir entre User (ROLE_USER) et Admin (ROLE_ADMIN)
7. Faire qu'au change sur le select, on déclenche la méthode changeRole
   Bonus : Côté spring, créer un enum UserRoles avec ROLE_USER et ROLE_ADMIN dedans et faire que la méthode pour changer le rôle attende cet enum en argument 

## 
